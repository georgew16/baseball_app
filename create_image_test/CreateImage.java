import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.awt.Image;
import javax.imageio.ImageIO;

public class CreateImage {
	static public void main(String args[]) throws Exception {
		try {
			// size of baseball_field image
			int width = 1032, height = 1038;

			// TYPE_INT_ARGB specifies the image format: 8-bit RGBA packed into integer pixels
			BufferedImage bi = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
			Graphics2D ig2 = bi.createGraphics();

			Image image = null;
			Image field = null;
			try {
				image = ImageIO.read(new File("baseball_tmp.png"));
			}
			catch (Exception ex) {
				System.out.println("No Image Found!");
			}
			// dummy coordinates used for testing
			ig2.drawImage(image, 212, 222, null);
			ig2.drawImage(image, 12, 22, null);

			ImageIO.write(bi, "PNG", new File("tmp.PNG"));

		} catch (IOException ie) {
			ie.printStackTrace();
		}
	}
}