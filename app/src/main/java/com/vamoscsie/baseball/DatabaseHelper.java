package com.vamoscsie.baseball;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.database.Cursor;
import java.util.ArrayList;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;

public class DatabaseHelper extends SQLiteOpenHelper{

    private static final String DATABASE_NAME = "BaseballStats.db";
    private static final String TABLE_NAME = "Stats";
    private static final String COL   = "ID";
    private static final String COL_1 = "Player_Name";
    private static final String COL_2 = "Play_Type";
    private static final String COL_3 = "X_Coordinate";
    private static final String COL_4 = "Y_Coordinate";

    private Gson gson = new Gson();


    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, 1);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_CONTACTS_TABLE = "CREATE TABLE " + TABLE_NAME + " ("
                + COL + " INTEGER PRIMARY KEY AUTOINCREMENT,"
                + COL_1 + " TEXT,"
                + COL_2 + " TEXT,"
                + COL_3 + " TEXT,"
                + COL_4 + " TEXT)";
        db.execSQL(CREATE_CONTACTS_TABLE);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_NAME);
        onCreate(db);
    }

    public String insertData(String name, String playType, String xCo, String yCo) {
        long result;


        try{
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues contentValues = new ContentValues();
            contentValues.put(COL_1, name);
            contentValues.put(COL_2, playType);
            contentValues.put(COL_3, toJson(xCo, "16"));    // NOTE: jsonArray needs at least 2 records in ARRAY, so 16 as dummy
            contentValues.put(COL_4, toJson(yCo, "16"));    // same as above
            result = db.insert(TABLE_NAME, null, contentValues);

        } catch(Exception e){
            return e.getMessage();
        }
        return result != (long) -1 ? "True" : "False";
    }

    // idx = 2 is X, 3 is Y
    public ArrayList<String> getPlayerData(String _name, int idx) {
        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query(TABLE_NAME, new String[] { COL_1,
                        COL_2, COL_3, COL_4 }, COL_1 + "=?",
                new String[] { _name }, null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
        return toArrayList(cursor.getString(idx));
    }

    // add new (X,Y) into DB.
    public String updatePlayer(String xCo, String yCo) {
        SQLiteDatabase db = this.getReadableDatabase();


        try {
            ContentValues values = new ContentValues();
            values.put(COL_1, "tmp");
            values.put(COL_2, "ground");

            ArrayList<String> prevX = getPlayerData("tmp", 2);  // 2 for X
            prevX.add(xCo);
            values.put(COL_3, toJson(prevX));

            ArrayList<String> prevY = getPlayerData("tmp", 3);  // 3 for Y
            prevY.add(yCo);
            values.put(COL_4, toJson(prevY));

            db.update(TABLE_NAME, values, COL_1 + " = ?", new String[]{"tmp"});
            return "Update Success!";
        } catch (Exception e) {
            return e.getMessage();
        }
    }

    public int getSize() {
        SQLiteDatabase db = this.getReadableDatabase();
        String countQuery = "SELECT  * FROM " + TABLE_NAME;
        Cursor cursor = db.rawQuery(countQuery, null);
        cursor.close();
        return cursor.getCount();
    }


    public void deleteData(String deleteID) {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_NAME, COL_1 + " = ?", new String[] {deleteID});
        db.close();
    }

    // get the string into Json String
    public String toJson(String... value) {
        ArrayList<String> inputArray = new ArrayList<>();
        for (String str : value) {
            inputArray.add(str);
        }
        return gson.toJson(inputArray);
    }

    // get the string array into Json String
    public String toJson(ArrayList<String> inputArray) {
        return gson.toJson(inputArray);
    }

    // convert from string to Json
    public ArrayList<String> toArrayList(String outputArray) {
        Type type = new TypeToken<ArrayList<String>>() {}.getType();

        ArrayList<String> ret = new ArrayList<>();
     //   ret.add(outputArray);

        try {
            ret = gson.fromJson(outputArray, type);
        } catch (Exception e) {
            ret.add(e.getMessage());
        }

        return ret;
    }








}
