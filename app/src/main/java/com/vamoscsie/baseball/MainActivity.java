package com.vamoscsie.baseball;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.content.Intent;
import android.widget.Toast;
import android.view.MotionEvent;
import android.content.DialogInterface;
import android.app.AlertDialog;
import java.util.ArrayList;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;

public class MainActivity extends AppCompatActivity {

    private ArrayList<String> ballInPlay;
    private DatabaseHelper myDB;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // set String data: "Ground" "Fly"
        initData();

        myDB = new DatabaseHelper(this);
        // clear the DB for testing, "tmp" is the dummy record's player_name
        myDB.deleteData("tmp");

        // dummy record for DB: "tmp" as player_name, every play is ground, last 2 are (x,y)
        // for testing purpose, insert first record, and future records are added/updated
        String tmp112 = myDB.insertData("tmp", "ground", "112", "122");

        // get the baseball field on display
        final ImageView baseballField = (ImageView) findViewById(R.id.baseballField);
        // wait for user to place the play's spot on screen
        baseballField.setOnTouchListener(new View.OnTouchListener() {

            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction()) {
                    case MotionEvent.ACTION_DOWN:
                        // place the position of play
                        placePlayPosition(event.getX(), event.getY());

                        // ask user for ground/fly ball
                        listDialog();

                        // store to DB
                        // String isInserted = myDB.insertData("tmp", "ground", Float.toString(event.getX()), Float.toString(event.getY()));

                        // for testing purpose, update to "tmp"
                        String isInserted = myDB.updatePlayer(Float.toString(event.getX()), Float.toString(event.getY()));
                        Toast.makeText(getApplicationContext(), isInserted,  Toast.LENGTH_SHORT).show();

                        /*
                        if (isInserted.equals("True")) {
                            Toast.makeText(getApplicationContext(), "Vamos!", Toast.LENGTH_SHORT).show();
                        }
                        else {
                            Toast.makeText(getApplicationContext(), "QQ", Toast.LENGTH_SHORT).show();
                        }
                        */


                        // retrieve data for checking
                        /*
                        ArrayList<String> ret = myDB.getPlayerData("tmp", 3);

                        String outputText = "";
                        for (String str : ret) {
                            outputText += (str + " ");
                        }
                        Toast.makeText(getApplicationContext(), outputText, Toast.LENGTH_SHORT).show();
                        */

                        break;
                    default:
                        break;
                }
                return true;
            }
        });


        // show all dots; FAILED
        final Button showAllBtn = (Button) findViewById(R.id.showAllBtn);
        showAllBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ArrayList<String> getX = myDB.getPlayerData("tmp", 2);
                ArrayList<String> getY = myDB.getPlayerData("tmp", 3);
                for (int i=2; i<5; i++) {

                    ImageView image = new ImageView(getApplicationContext());

                    int x = (int) Float.parseFloat(getX.get(i));
                    int y = (int) Float.parseFloat(getY.get(i));
                    LayoutParams params = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                    params.setMargins(x, y, x+32, y+32);
                    image.setLayoutParams(params);
                    image.setImageResource(R.drawable.x_mark);




                    ImageView image2 = new ImageView(getApplicationContext());

                    int x2 = (int) Float.parseFloat(getX.get(i+1));
                    int y2 = (int) Float.parseFloat(getY.get(i+1));
                    LayoutParams params2 = new LayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
                    params.setMargins(x2, y2, x+32, y+32);
                    image2.setLayoutParams(params2);
                    image2.setImageResource(R.drawable.x_mark_tmp);


                    linear1.addView(image);
                    linear1.addView(image2);
                    Toast.makeText(getApplicationContext(), getX.get(i) + " " + getY.get(i), Toast.LENGTH_SHORT).show();
                }
            }
        });


    }

    // place the play on screen
    private void placePlayPosition(float _x, float _y) {
        ImageView positionMark;
        positionMark = (ImageView) findViewById(R.id.x_mark);

        int x = (int)_x;
        int y = (int)_y;

        positionMark.layout(x, y, x+32, y+32);
        positionMark.setVisibility(0);
    }

    private void initData() {
        ballInPlay = new ArrayList<>();
        ballInPlay.add(getString(R.string.type1));
        ballInPlay.add(getString(R.string.type2));
    }

    // ask for Ground/Fly Ball
    private void listDialog() {
        new AlertDialog.Builder(MainActivity.this)
            .setItems(ballInPlay.toArray(new String[ballInPlay.size()]), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    String name = ballInPlay.get(which);
                    Toast.makeText(getApplicationContext(), getString(R.string.thePlay) + " " + name, Toast.LENGTH_SHORT).show();
                }
            }).show();
    }

}
